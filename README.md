---
Metadata
Title: Tα πρώτα μου βήματα στα Linux containers. Lxc, Lxd στα ελληνικά. Μέρος 1.
Alternative Title: My first steps in Linux containers. Lxc, Lxd in Hellenic. Part 1.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 24/06/2022
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: First basic commands for creating linux containers from a beginner - intermediate user, step by step.
Category: Άρθρα και οδηγοί
Tags: Lxc, Lxd, containers

---

Άδεια: Creative Commons - Αναφορά Δημιουργού 4.0 Διεθνές (CC BY 4.0)

![linux containers](https://gitlab.com/nikaskonstantinos/linux-containers-in-hellenic/-/raw/main/lxc001.png)

## Εισαγωγή
 Τα linux containers είναι σαν ξεχωριστά λειτουργικά συστήματα, εγκατεστημένα στο σύστημα που τα φιλοξενεί. Μπορούν να δημιουργηθούν σχετικά εύκολα από έναν μέσο χρήστη σε λειτουργικό σύστημα gnu/linux αξιοποιώντας την αρχιτεκτονική και την "καρδιά" του συστήματος που τα φιλοξενεί, με αποτέλεσμα να καταναλώνουν λίγους πόρους και να εκτελούν λειτουργίες, σχεδόν σαν φυσικές διανομές.
 
 ## Εγκατάσταση
 Αρχικά εγκαθιστούμε το `snapd`  ανάλογα με τη διανομή μας, για να εγκαταστήσουμε την εντολή `lxd` για την αρχική διαχείριση και έπειτα χρησιμοποιούμε ως επί το πλείστον την εντολή `lxc`.  Το παράδειγμα εφαρμόζεται σε φορητό υπολογιστή με Pop!_Os 22.04.
 
 Τα βήματα που ακολουθούν μπορούν να χρησιμοποιηθούν και από έναν αρχάριο χρήστη, χωρίς να υπάρχει κίνδυνος να καταστρέψουν τη διαμόρφωση του λειτουργικού του συστήματος.
 Πάμε!!!
 
1.  `sudo apt install snapd`
 
 2.  `snap install lxd`
 
 3.  `sudo usermod -aG lxd $USER`
 
 Επανεκκίνηση του υπολογιστή.
 
 4. `lxd init`
 
 Πατάμε `enter`  σε όλες τις επιλογές που εμφανίζονται για το περιβάλλον ανάπτυξης των containers, εκτός εάν είμαστε προχωρημένοι χρήστες που επιδιώκουμε κάτι συγκεκριμένο.
  
  Τέλος, αυτό ήταν! H εγκατάσταση ολοκληρώθηκε. Και τώρα;
 ## Χρήση του lxc
 
 Οι πηγές των linux containes:
 
5. `lxc remote list`
 
 Οι υπάρχουσες "διανομές" σε images:
 
6. `lxc image list images:`
 
Οι υπάρχουσες Devuan για παράδειγμα εικόνες:

7. `lxc image list images:devuan`

### Δημιουργία Linux container

8. `lxc launch images:devuan/chimaera`

Περιμένουμε λίγο, και το   container devuan με την τυχαία ονομασία είναι έτοιμο. Αν θέλουμε να επιλέξουμε εμείς την ονομασία την προσθέτουμε μετά από ένα κενό διάστημα στο τέλος της εντολής 8.

9. `lxc list`

Θα κουραστούμε να τη βλέπουμε. Μας εμφανίζει το "σμήνος" από τα διαθέσιμα  containers  με τις διευθύνσεις τους και τις ονομασίες τους. Αυτή τη στιγμή έχουμε μόνο ένα, με την τυχαία ονομασία του.

### Είσοδος στο container

10. `lxc exec <ονομασία του container χωρίς τα άγκιστρα> bash`

ή 
(`lxc exec <ονομασία του container χωρίς τα άγκιστρα> -- /bin/bash`
 
αντίστοιχα εάν προερχόταν από εικόνα alpine, η εντολή θα ήταν `lxc exec <apline πχ> sh`, γιατί η alpine έχει dash  και όχι bash και αντί για  `bash` στο τέλος βάζουμε `sh`.)
 
Δοκιμάζουμε μία ενημέρωση, θυμηθείτε βρισκόμαστε στη Devuan/Chimaera:

11. `sudo apt-get update`
δεν πειράζει που έμαστε στον `root`
 
Δοκιμάζουμε την εγκατάσταση μιας εφαρμογής που λέγεται `neofetch` και μας δείχνει τα στοιχεία του συστήματός μας και άλλες πληροφορίες:
 
12. `sudo apt-get install neofetch`
 
Εκτελούμε την εφαρμογή `neofetch`:
  
13. `neofetch`
  
Αν βλέπετε την παρακάτω εικόνα, τα καταφέρατε!
  ```
  root@< ονομασία>:~# neofetch
   ..,,;;;::;,..                   root@<ονομασία> 
           `':ddd;:,.              --------------------- 
                 `'dPPd:,.         OS: Devuan GNU/Linux 4 (chimaera) x86_64 
                     `:b$$b`.      Host: 80L0 Lenovo G50-80 
                        'P$$$d`    Kernel: 5.17.5-76051705-generic 
                         .$$$$$`   Uptime: 7 mins 
                         ;$$$$$P   Packages: 187 (dpkg) 
                      .:P$$$$$$`   Shell: bash 5.1.4 
                  .,:b$$$$$$$;'    Resolution: 1366x768 
             .,:dP$$$$$$$$b:'      CPU: Intel i3-4030U (4) @ 1.800GHz 
      .,:;db$$$$$$$$$$Pd'`         Memory: 8MiB / 15924MiB 
 ,db$$$$$$$$$$$$$$b:'`
:$$$$$$$$$$$$b:'`                                          
 `$$$$$bd:''`                                              
   `'''`

 
```
 
Ας τρέξουμε τώρα έναν διακομιστή nginx:
 
14. `sudo apt-get install nginx -y `
 
 
Βγαίνουμε από το container εν κινήσει:
 
15. `exit`

Βλέπουμε την IPV4, με τη γνωστή  εντολή

16. `lxc list`

Aντιγράφουμε την διεύθυνση IPV4 χωρίς το `(eth0)`,  και κάνουμε επικόλληση και αναζήτηση στον φυλλομετρητή μας.

Αν βλέπετε κάτι  σχετικό τα καταφέρατε!

![Αρχική σελίδα διακομιστή nginx](https://gitlab.com/nikaskonstantinos/linux-containers-in-hellenic/-/raw/main/lxc01.png)

Εικόνα 1. Αρχική σελίδα διακομιστής nginx

## Επίλογος 

Αν θέλουμε περισσότερες πληροφορίες για το container:

17. `lxc config show <ονομασία του container χωρίς τα άγκιστρα>`


Αν θέλουμε να σταματήσουμε το container:

18. `lxc stop <όνομα και μη χωριό>`

αν θέλουμε να το ξεκινήσουμε `start`, ή `restart` ανάλογα.

Αν θέλουμε να σβήσουμε ΟΡΙΣΤΙΚΑ, (προσοχή),  το  container:

19. `lxc delete <ονομασία>`

Τέλος διαπιστώνουμε την κατάσταση των containers, αν υπάρχουν, με τη γνωστή πλέον εντολή:

20. `lxc list`

Καλή επιτυχία!!!
 
 ## Πηγές
 
 1. https://support.system76.com/articles/containers/
 2. https://www.learnlinux.tv/getting-started-with-lxd-containerization/
 3. https://discuss.linuxcontainers.org/
 
 
 
 
